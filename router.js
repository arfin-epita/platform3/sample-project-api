/******************************************************
 *          THIS IS THE MAIN ROUTER                   *
 ******************************************************/
const express = require('express');
const router = express.Router();


/* MODULES ROUTERS BEGIN */
const listRouter = require('./src/router/index');

router.get("/ping", (req, res, next) =>
{
  return res.send("Hello world !");
});


router.use(listRouter);

module.exports = router;
