FROM node:10-alpine

RUN mkdir -p /usr/src/app/
WORKDIR /user/src/app/

COPY package.json /user/src/app/

RUN npm install
#RUN npm i npm@latest

COPY . /user/src/app/

#EXPOSE

ENTRYPOINT [ "npm", "run", "startProd" ]
