const mongoose = require('mongoose');

const ActivitiesSchema = new mongoose.Schema(
{
  content: {type: String, default: null},
  date: {type: Date, default: Date.now()}
},
{
  collection : 'activities',
  strict: true
});


const Activities = mongoose.model('Activities', ActivitiesSchema);

module.exports = {Activities};

