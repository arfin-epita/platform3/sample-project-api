// Handlers
const logHandler = require('@logHandler');
const errorHandler = require('@errorHandler');

// Model
const {Activities} = require('../models/Activities');



function findActivities()
{
  return new Promise((resolve, reject) =>
  {
    Activities
    .find({})
    .exec()
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(errorHandler.cast(err));
    });
  });
}


function insert(obj)
{
  return new Promise((resolve, reject) =>
  {
    Activities
    .create(obj)
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    })
  })
}


function deleteByID(id)
{
  return new Promise((resolve, reject) =>
  {
    Activities
    .findByIdAndDelete(id)
    .then(data =>
    {
      return resolve(data);
    })
    .catch(err =>
    {
      return reject(err);
    });
  })
}

module.exports =
{
  findActivities,
  insert,
  deleteByID
};